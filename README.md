# New Software Technologies 2#

This is project for New Software Technologies class for Software Engineering lab at Faculty of Organisation Science, Belgrade University. 

### Technologies used###

* Spring Boot
* Spring DATA JPA
* MySQL, HSQL (testing)
* Spring Security
* Tymeleaf
* Bootstrap

### How do I get set up? ###

* Create database from nst2.sql file from the project.
* If it is needed, change database connection credentials in application.properties file
* Copy files from imgs folder to C:\imgs at your local disc drive.
* Build and run, then open website at http://localhost:8080