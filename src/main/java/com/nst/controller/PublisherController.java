package com.nst.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nst.model.Book;
import com.nst.model.Category;
import com.nst.model.Publisher;
import com.nst.model.User;
import com.nst.service.BookService;
import com.nst.service.CategoryService;
import com.nst.service.PublisherService;
import com.nst.service.UserService;

@Controller
public class PublisherController {

	@Autowired
	private PublisherService publisherService;

	@Autowired
	private UserService userService;

	@Autowired
	private BookService bookService;

	@Autowired
	private CategoryService categoryService;


	@RequestMapping(value="/add_publisher", method = RequestMethod.GET)
	public String addPublisher(Model model){
		model.addAttribute(new Publisher());
		return "add_publisher";
	}

	@RequestMapping(value="/action/add_publisher", method = RequestMethod.POST)
	public String addPublisher(@Valid @ModelAttribute Publisher publisher, BindingResult bindingResult,  Model model){

		if(bindingResult.hasErrors()){
			return "add_publisher";
		}

		Publisher publisherForSave = setUpPublisher(publisher);

		Publisher savedPublisher = publisherService.save(publisherForSave); 

		model.addAttribute(savedPublisher);
		model.addAttribute("book", new Book());
		model.addAllAttributes(getAllBooks());
		model.addAttribute("popularCategories", getPopularCategories());

		if(savedPublisher == null){
			return "add_publisher";
		}		

		return "redirect:/index";
	}

	@RequestMapping(value = ("/publishers"), method = RequestMethod.GET)
	public String categories(Model model){
		List<Publisher> publishers = publisherService.findAll();
		model.addAttribute("publishers", publishers);
		model.addAttribute("book", new Book());
		model.addAttribute("popularCategories", getPopularCategories());
		return "all_publishers";
	}


	@RequestMapping(value ="publisher", method = RequestMethod.GET)
	public String view(Model model, long id){
		Publisher publisher = publisherService.findById(id);

		List<Book> books = getAlLBooksForPublisher(publisher);

		// Add models
		model.addAllAttributes(getPopularBooks());
		model.addAttribute("publisherBooks", books);
		model.addAttribute("publisher", publisher);
		model.addAttribute("book", new Book());
		model.addAttribute("popularCategories", getPopularCategories());

		return "publisher";
	}

	@RequestMapping(value ="/action/deletePublisher", method = RequestMethod.GET)
	public String deleteBook(Model model, int id){
		Publisher editPublisher = publisherService.findById(id);
		publisherService.delete(editPublisher);
		return "redirect:/index";
	}

	@RequestMapping(value ="/editPublisher", method = RequestMethod.GET)
	public String editBook(Model model, int id){
		Publisher editPublisher = publisherService.findById(id);
		model.addAttribute("publisher", editPublisher);
		return "edit_publisher";		
	}	

	@ModelAttribute("books")
	public List<Book> getAllBooks() {
		return bookService.findAll();
	}

	@ModelAttribute("popularBooks")
	public List<Book> getPopularBooks() {
		return bookService.getPopularBooks();
	}

	@ModelAttribute("popularCategories")
	public List<Category> getPopularCategories() {
		return categoryService.find5Categories();
	}	

	private List<Book> getAlLBooksForPublisher(Publisher publisher) {
		//Find books for this category
		List<Book> books = new ArrayList<>();
		for(Book b : publisher.getBookList()){
			books.add(b);
		}
		return books;
	}	

	private Publisher setUpPublisher(Publisher publisher) {
		User currentUser = userService.getLoggedInUser(true);
		publisher.setUser(currentUser);
		return publisher;
	}

}
