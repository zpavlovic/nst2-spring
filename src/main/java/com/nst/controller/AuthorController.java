package com.nst.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.nst.model.Author;
import com.nst.model.Book;
import com.nst.model.Category;
import com.nst.model.User;
import com.nst.service.AuthorService;
import com.nst.service.BookService;
import com.nst.service.CategoryService;
import com.nst.service.UserService;

@Controller
public class AuthorController {
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BookService bookService;
	
	@Autowired
	private CategoryService categoryService;
	
	public static final String ROOT = "C:\\imgs";
	
	
	@RequestMapping(value="add_author", method = RequestMethod.GET)
	public String addAuthor(Model model){
		model.addAttribute(new Author());
		return "add_author";
	}
	
	@RequestMapping(value="action/add_author", method = RequestMethod.POST)
	public String addAuthor(@Valid @ModelAttribute Author author, BindingResult result, MultipartFile file,  Model model){
		
		if(result.hasErrors()){
			 List<FieldError> errors = result.getFieldErrors();
			    for (FieldError error : errors ) {
			        System.out.println (error.getObjectName() + " - " + error.getDefaultMessage());
			    }
			return "add_author";
		}
		
		Author authorForSave = handleAuthor(author, file);	
		
		Author savedAuthor = authorService.save(authorForSave); 
		
		if(savedAuthor == null){
			return "add_author";
		}	
				
		model.addAttribute(author);
		model.addAttribute("book", new Book());
		model.addAllAttributes(getAllBooks());
		
		return "redirect:/index";
	}	
		
	@RequestMapping(value ="/action/deleteAuthor", method = RequestMethod.GET)
	public String deleteAuthor(Model model, int id){
		Author editAuthor = authorService.findById(id);
		authorService.delete(editAuthor);
		return "redirect:/index";		
	}
	
	@RequestMapping(value ="/editAuthor", method = RequestMethod.GET)
	public String editAuthor(Model model, int id){
		Author editAuthor = authorService.findById(id);
		model.addAttribute("author", editAuthor);
		return "edit_author";		
	}

	@RequestMapping(value ="author", method = RequestMethod.GET)
	public String author(Model model, long id){	
		Author author = authorService.findById(id);		
		model.addAllAttributes(getPopularBooks());
		model.addAttribute("authorBooks", booksForAuthor(author));
		model.addAttribute("author", author);
		model.addAttribute("book", new Book());
		model.addAttribute("popularCategories", getPopularCategories());		
		return "author";
	}
	
	@RequestMapping(value = ("/authors"), method = RequestMethod.GET)
	public String authors(Model model){
		List<Author> authors = authorService.findAll();
		model.addAttribute("authors", authors);
		model.addAttribute(new Book());
		model.addAttribute("popularCategories", getPopularCategories());
		return "all_authors";
	}
	
	@ModelAttribute("popularBooks")
	public List<Book> getPopularBooks() {
		return bookService.getPopularBooks();
	}
	
	@ModelAttribute("allAuthors")
	public List<Author> getAllAuthors() {
		return authorService.findAll();
	}
	
	@ModelAttribute("books")
	public List<Book> getAllBooks() {
		return bookService.findAll();
	}
	
	@ModelAttribute("popularCategories")
	public List<Category> getPopularCategories() {
		return categoryService.find5Categories();
	}	

	private Author handleAuthor(Author author, MultipartFile file) {
		addImageToAuthor(author, file);	
		User currentUser = userService.getLoggedInUser(true);
		author.setUser(currentUser);
		return author;
	}

	private void addImageToAuthor(Author author, MultipartFile file) {
		if (!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(), Paths.get(ROOT, file.getOriginalFilename()));
				author.setImage(file.getOriginalFilename());
				System.out.println("LOKACIJA: "+ROOT+"/"+file.getOriginalFilename());
			} catch (IOException|RuntimeException e) {
				System.out.println(e.getMessage());
			}
		} 		
	}	

	private List<Book> booksForAuthor(Author author) {
		//Find books for this author
		List<Book> books = new ArrayList<>();
		for(Book b : author.getBookList()){
			books.add(b);
		}
		return books;
	}	
}
