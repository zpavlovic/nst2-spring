package com.nst.controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.basic.BasicOptionPaneUI;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nst.model.Book;
import com.nst.model.Category;
import com.nst.model.User;
import com.nst.service.BookService;
import com.nst.service.CategoryService;
import com.nst.service.UserService;

@Controller
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BookService bookService;
	
	
	@RequestMapping(value="/add_category", method = RequestMethod.GET)
	public String addCategory(Model model){
		model.addAttribute(new Category());
		return "add_category";
	}
	
	@RequestMapping(value="/action/add_category", method = RequestMethod.POST)
	public String addCategory(@Valid @ModelAttribute Category category, BindingResult bindingResult,  Model model){
		
		if(bindingResult.hasErrors()){
			return "add_category";
		}	
		
		Category categoryForSave = setUpCategory(category);
		
		Category savedCategory = categoryService.save(categoryForSave); 
		
		model.addAttribute(category);
		model.addAttribute("book", new Book());
		model.addAllAttributes(getAllBooks());
		
		if(savedCategory == null){
			return "add_category";
		}		
		
		return "redirect:/index";
	}	

	@RequestMapping(value ="category", method = RequestMethod.GET)
	public String category(Model model, long id){
		Category category = categoryService.findById(id);
		
		//Find books for this category
		List<Book> books = getBooksForCategory(category);
		
		// Add models
		model.addAllAttributes(getPopularBooks());
		model.addAttribute("categoryBooks", books);
		model.addAttribute("category", category);
		model.addAttribute("book", new Book());
		model.addAttribute("popularCategories", getPopularCategories());
		return "category";
	}

	@RequestMapping(value ="/action/deleteCategory", method = RequestMethod.GET)
	public String deleteBook(Model model, int id){
		Category editCategory = categoryService.findById(id);
		categoryService.delete(editCategory);
		return "redirect:/index";		
	}
	
	@RequestMapping(value ="/editCategory", method = RequestMethod.GET)
	public String editBook(Model model, int id){
		Category editCategory = categoryService.findById(id);
		model.addAttribute("category", editCategory);
		return "edit_category";		
	}
	
	@RequestMapping(value = ("/categories"), method = RequestMethod.GET)
	public String categories(Model model){
		List<Category> categories = categoryService.findAll();
		model.addAttribute("categories", categories);
		model.addAttribute(new Book());
		model.addAttribute("popularCategories", getPopularCategories());
		return "all_categories";
	}
	
	@ModelAttribute("books")
	public List<Book> getAllBooks() {
		return bookService.findAll();
	}
	
	@ModelAttribute("popularBooks")
	public List<Book> getPopularBooks() {
		return bookService.getPopularBooks();
	}
	
	@ModelAttribute("popularCategories")
	public List<Category> getPopularCategories() {
		return categoryService.find5Categories();
	}
	
	private Category setUpCategory(Category category) {
		User currentUser = userService.getLoggedInUser(true);		
		category.setUser(currentUser);
		return category;
	}
		
	private List<Book> getBooksForCategory(Category category) {
		List<Book> books = new ArrayList<>();
		for(Book b : category.getBookList()){
			books.add(b);
		}
		return books;
	}
}
