package com.nst.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nst.model.Book;
import com.nst.model.Category;
import com.nst.model.User;
import com.nst.service.BookService;
import com.nst.service.CategoryService;
import com.nst.service.UserService;


@Controller
public class IndexController {
	
	@Autowired
	BookService bookService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	CategoryService categoryService;
	
	
	@RequestMapping(value = "/")
	public String index(Model model){		
		model.addAttribute("book", new Book());
		model.addAttribute("books", displayAllBooks());
		model.addAttribute("popularBooks", getPopularBooks());		
		model.addAttribute("popularCategories", getPopularCategories());
		return "index";
	}
	
	@RequestMapping(value = "/index")
	public String showIndex(Model model){
		model.addAttribute("book", new Book());
		model.addAttribute("books", displayAllBooks());
		model.addAttribute("popularBooks", getPopularBooks());
		model.addAttribute("popularCategories", getPopularCategories());
		return "index";
	}
	
	@RequestMapping(value="/admin")
	public String login(Model model){
		model.addAttribute(new Book());
		return "admin";
	}
	
	
	@ModelAttribute("books")
	public List<Book> displayAllBooks() {
		return bookService.findAll();
	}
	

	@ModelAttribute("popularBooks")
	public List<Book> getPopularBooks() {
		return bookService.getPopularBooks();
	}
	
	@ModelAttribute("popularCategories")
	public List<Category> getPopularCategories() {
		return categoryService.find5Categories();
	}
	
	@ModelAttribute("user")
	public User getLoggedIn(){
		return userService.getLoggedInUser(false);
	}
}
