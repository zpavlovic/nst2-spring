package com.nst.controller;



import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.nst.model.Author;
import com.nst.model.Book;
import com.nst.model.Category;
import com.nst.model.Publisher;
import com.nst.model.User;
import com.nst.service.AuthorService;
import com.nst.service.BookService;
import com.nst.service.CategoryService;
import com.nst.service.PublisherService;
import com.nst.service.UserService;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;

	@Autowired
	private UserService userService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private PublisherService publisherService;

	public static final String ROOT = "C:\\imgs";

	private final ResourceLoader resourceLoader;

	@Autowired
	public BookController(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	@RequestMapping(value="add_book", method = RequestMethod.GET)
	public String addAuthor(Model model){
		model.addAttribute(new Book());
		model.addAllAttributes(displayAllAuthors());
		return "add_book";
	}

	@RequestMapping(value="action/add_book", method = RequestMethod.POST)
	public String addBook(@Valid @ModelAttribute Book book, BindingResult result, MultipartFile file, Model model){		
		if(result.hasErrors()){
			return "add_book";
		}
		
		Book bookForSave = setUpBook(book, result, file);
		Book savedBook = saveOrUpdateAndAddImage(bookForSave, file);

		model.addAttribute(savedBook);
		model.addAllAttributes(displayAllBooks());

		if(savedBook == null){
			return "add_book";
		}		

		return "redirect:/index";
	}

	@RequestMapping(value = "/image/{imageName}")
	@ResponseBody
	public byte[] getFile(@PathVariable String imageName) throws IOException {		   
		Resource resource = resourceLoader.getResource("file:" + Paths.get(ROOT, imageName+".jpg"));
		File file = resource.getFile();	
		return Files.readAllBytes(file.toPath());
	}

	public Book saveOrUpdateAndAddImage(Book book, MultipartFile file){
		Book bookForSave = handleBookImage(book, file);		
		Book savedBook = bookService.saveBook(bookForSave);		
		return savedBook;
	}

	@RequestMapping(value="/search", method = RequestMethod.POST)
	public String searchBook(@RequestParam String name, Model model){
		try {
			List<Book> searchBooks = bookService.search(name);
			model.addAttribute("searchBooks",searchBooks);
			model.addAllAttributes(getPopularBooks());
			model.addAttribute("book", new Book());
			model.addAttribute("popularCategories", getPopularCategories());
			return "search";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			model.addAttribute("book", new Book());
			model.addAttribute("error", e.getMessage());
			model.addAttribute("popularCategories", getPopularCategories());
			return "search";
		}			
	}

	@RequestMapping(value ="book", method = RequestMethod.GET)
	public String book(Model model, int id){
		Book book = increaseBookViews(id);		
		Book savedBook = bookService.saveBook(book);		
		List<Author> authors = getAllAuthorsForBook(savedBook);		
		List<Category> categories = getAllCategoriesForBook(savedBook);

		// Add models
		model.addAllAttributes(getPopularBooks());
		model.addAllAttributes(getRecommendBooks());
		model.addAttribute("book", savedBook);
		model.addAttribute("authors", authors);
		model.addAttribute("categories", categories);
		model.addAttribute("publisher", savedBook.getPublisher());
		model.addAttribute("popularCategories", getPopularCategories());

		return "book";
	}

	@RequestMapping(value ="/action/deleteBook", method = RequestMethod.GET)
	public String deleteBook(Model model, int id){
		Book deleteBook = bookService.findById(id);
		bookService.delete(deleteBook);
		return "redirect:/index";
	}

	@RequestMapping(value ="/editBook", method = RequestMethod.GET)
	public String editBook(Model model, int id){
		Book editBook = bookService.findById(id);
		model.addAttribute("book", editBook);
		return "edit_book";		
	}

	@ModelAttribute("authors")
	public List<Author> displayAllAuthors() {
		return authorService.findAll();
	}

	@ModelAttribute("categories")
	public List<Category> displayAllCategories() {
		return categoryService.findAll();
	}	
	
	@ModelAttribute("publishers")
	public List<Publisher> displayAllPublishers() {
		return publisherService.findAll();
	}	

	@ModelAttribute("books")
	public List<Book> displayAllBooks() {
		return bookService.findAll();
	}

	@ModelAttribute("popularCategories")
	public List<Category> getPopularCategories() {
		return categoryService.find5Categories();
	}

	@ModelAttribute("popularBooks")
	public List<Book> getPopularBooks() {
		return bookService.getPopularBooks();
	}

	@ModelAttribute("recommendBooks")
	public List<Book> getRecommendBooks() {
		return bookService.reccommendBooks();
	}
	
	private Book handleBookImage(Book book, MultipartFile file) {
		if (!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(), Paths.get(ROOT, file.getOriginalFilename()));
				book.setImage(file.getOriginalFilename());
				System.out.println("LOKACIJA: "+ROOT+"/"+file.getOriginalFilename());
			} catch (IOException|RuntimeException e) {
				System.out.println(e.getMessage());
			}
		}
		return book;
	}

	private Book setUpBook(Book book, BindingResult result, MultipartFile file) {		
		User currentUser = userService.getLoggedInUser(true);
		book.setUser(currentUser);
		book.setAddedDate(getAddedDate());
		return book;
	}
	
	private Date getAddedDate() {
		Calendar calendar = Calendar.getInstance();
		java.sql.Date date = new java.sql.Date(calendar.getTime().getTime());    
		return date;
	}
	

	private Book increaseBookViews(int id) {
		Book book = bookService.findById(id);
		long views = book.getViews();
		views++;
		book.setViews(views);
		return book;
	}

	private List<Category> getAllCategoriesForBook(Book savedBook) {
		//Get all categories for this book
		List<Category> categories = new ArrayList<>();

		for(Category c : savedBook.getCategoryList()){
			categories.add(c);
		}
		return categories;
	}

	private List<Author> getAllAuthorsForBook(Book savedBook) {
		//Get all authors for this book
		List<Author> authors = new ArrayList<>();

		for(Author a : savedBook.getAuthorsList()){
			authors.add(a);
		}
		return authors;
	}
}
