package com.nst.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.mysql.fabric.xmlrpc.base.Array;


@Entity
@Table(name="author")
public class Author {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@NotNull
	@Column
	private long id;

	@Size(min = 1,message = "First name can not be emty")
	@NotNull
	@Column(name = "first_name")
	private String firstName;

	@Size(min = 1,message = "Last name can not be emty")
	@NotNull
	@Column(name = "last_name")
	private String lastName;

	@Column
	private String description;

	@Column
	private String image;

	@Column
	private String website;

	@JoinColumn(name = "user_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private User user;
	
	@ManyToMany(mappedBy = "authorsList")
	private List<Book> bookList = new ArrayList<>();
	
	public Author() {
		// TODO Auto-generated constructor stub
	}
	
	public Author(long id, String firstName, String lastName, String description, User user) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.user = user;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public void setBookList(List<Book> bookList) {
		this.bookList = bookList;
	}
	
	public List<Book> getBookList() {
		return bookList;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Author)) {
			return false;
		}
		Author other = (Author) object;			
		if(other.id != this.id){
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int hash = 5;
		hash = 83 * hash + Objects.hashCode(this.id);
		return hash;
	}


}
