package com.nst;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;

import com.nst.service.UserService;


@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;		

	@Value("${app.secret}")
	private String applicationSecret;

	private int YEAR_IN_SECONDS = 31536000;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
		.antMatchers("/", "/index","/action/search", "/search", "book", "category", "/categories", "/author","/authors",
				"/publisher", "/publishers", "/css/**", "/img/**", "/image/**", "/templates/**", "/webjars/**").permitAll()
		.anyRequest().authenticated();
		http
		.formLogin()
		.loginPage("/admin")
		.permitAll()
		.and()   
		.logout()
		.permitAll()
		.and()
		.rememberMe().key(applicationSecret)
		.tokenValiditySeconds(YEAR_IN_SECONDS);		
		
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
	     final HttpSecurity http = getHttp();
	        web.postBuildAction(new Runnable() {
	            @Override
	            public void run() {
	                web.securityInterceptor(http.getSharedObject(FilterSecurityInterceptor.class));
	            }
	        });
	}
}