package com.nst.service;

import java.util.List;

import com.nst.model.Author;
import com.nst.model.Book;

public interface AuthorService {
	
	Author save(Author author);

	void delete(Author author);

	Author findOne(Author author);

	List<Author> findAll();

	Author findById(long id);

}
