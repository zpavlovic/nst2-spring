package com.nst.service;

import java.util.List;

import com.nst.model.Publisher;

public interface PublisherService {
	
	Publisher save(Publisher publisher);
	
	void delete(Publisher publisher);
	
	Publisher findOne(Publisher publisher);
	
	List<Publisher> findAll();
	
	Publisher findById(long id);

}
