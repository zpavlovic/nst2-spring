package com.nst.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import com.nst.model.User;



public interface UserService extends UserDetailsService {
	
	User findByUsername(String username);
	
	User register(User user);
	
	void autoLogin(User user);
	
	User getLoggedInUser(Boolean forceFresh);

	User save(User user);
	
}
