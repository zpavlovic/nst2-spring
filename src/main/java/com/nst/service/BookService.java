package com.nst.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.nst.model.Author;
import com.nst.model.Book;

public interface BookService {
	
	Book saveBook(Book book);

	void delete(Book book);

	Book findOne(Book book);
	
	List<Book> findAll();

	List<Book> search(String value) throws Exception;

	Book findById(long id);

	List<Book> getPopularBooks();
	
	List<Book> reccommendBooks();

}
