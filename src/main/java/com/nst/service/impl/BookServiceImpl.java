package com.nst.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nst.model.Book;
import com.nst.repository.BookRepository;
import com.nst.service.BookService;

import javassist.NotFoundException;

@Service
@Transactional
public class BookServiceImpl implements BookService {
	
	@Autowired
	private BookRepository bookRepository;

	@Override
	public Book saveBook(Book book) {
		Book saveBook = bookRepository.save(book);
		return saveBook;
	}

	@Override
	public void delete(Book book) {
		bookRepository.delete(book);
		
	}

	@Override
	public Book findOne(Book book) {
		return bookRepository.findOne(book.getId());
	}

	@Override
	public List<Book> findAll() {
		return bookRepository.findAll();
	}

	@Override
	public List<Book> search(String value) throws Exception {
		
		List<Book> books = bookRepository.findByNameContaining(value);
		
		if(books == null){
			throw new NotFoundException("Book not found");
		}
		
		if(books.isEmpty()){
			throw new Exception("Book not found");
		}
		
		return books;
	}

	@Override
	public Book findById(long id) {
		return bookRepository.findById(id);
	}

	@Override
	public List<Book> getPopularBooks() {
		return bookRepository.getPopularBooks(new PageRequest(0,5));
	}

	@Override
	public List<Book> reccommendBooks() {
		return bookRepository.reccommendBooks(new PageRequest(0, 3));
	}

}
