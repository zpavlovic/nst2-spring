package com.nst.service.impl;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nst.model.User;
import com.nst.repository.UserRepository;
import com.nst.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private HttpSession httpSession;

	public final String CURRENT_USER_KEY = "CURRENT_USER";

	@Override
	public User findByUsername(String username) {
		User u = userRepository.findByUsername(username);
		return u;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		return user;
	}

	@Override
	public User register(User user) {
		user.setPassword(encodeUserPassword(user.getPassword()));

		if (this.userRepository.findByUsername(user.getUsername()) == null) {
			this.userRepository.save(user);
			return user;
		}

		return null;
	}


	@Override
	public void autoLogin(User user) {
		autoLogin(user.getUsername());
	}

	public void autoLogin(String username) {
		UserDetails userDetails = this.loadUserByUsername(username);
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken (userDetails, null, userDetails.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);
		if(auth.isAuthenticated()) {
			SecurityContextHolder.getContext().setAuthentication(auth);
		}
	}

	public String encodeUserPassword(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}

	@Override
	public User getLoggedInUser(Boolean forceFresh) {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = (User) httpSession.getAttribute(CURRENT_USER_KEY);
		if(forceFresh || httpSession.getAttribute(CURRENT_USER_KEY) == null) {
			user = this.userRepository.findByUsername(userName);
			httpSession.setAttribute(CURRENT_USER_KEY, user);
		}
		return user;
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}
}
