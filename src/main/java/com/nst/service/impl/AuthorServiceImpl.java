package com.nst.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nst.model.Author;
import com.nst.model.Book;
import com.nst.repository.AuthorRepository;
import com.nst.service.AuthorService;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {
	
	@Autowired
	private AuthorRepository authorRepository;

	@Override
	public Author save(Author author) {
		return authorRepository.save(author);
	}


	@Override
	public void delete(Author author) {
		authorRepository.delete(author);		
	}

	@Override
	public Author findOne(Author author) {
		return authorRepository.findOne(author.getId());
	}


	@Override
	public List<Author> findAll() {
		return authorRepository.findAll();
	}


	@Override
	public Author findById(long id) {
		return authorRepository.findById(id);
	}

}
