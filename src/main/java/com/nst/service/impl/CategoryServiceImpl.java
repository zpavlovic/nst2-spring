package com.nst.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nst.model.Category;
import com.nst.repository.CategoryRepository;
import com.nst.service.CategoryService;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public Category save(Category category) {
		Category c = categoryRepository.save(category);
		return c;
	}

	@Override
	public void delete(Category category) {
		categoryRepository.delete(category);		
	}

	@Override
	public Category findOne(Category category) {
		return categoryRepository.findOne(category.getId());
	}

	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	@Override
	public Category findById(long id) {
		return categoryRepository.findById(id);
	}

	@Override
	public List<Category> find5Categories() {
		return categoryRepository.find5Categories(new PageRequest(0, 10));
	}

}
