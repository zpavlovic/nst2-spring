package com.nst.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nst.model.Publisher;
import com.nst.repository.PublisherRepository;
import com.nst.service.PublisherService;

@Service
@Transactional
public class PublisherServiceImpl implements PublisherService {
	
	@Autowired
	public PublisherRepository publisherRepository;

	@Override
	public Publisher save(Publisher publisher) {
		Publisher p = publisherRepository.save(publisher);
		return p;
	}

	@Override
	public void delete(Publisher publisher) {
		publisherRepository.delete(publisher.getId());
		
	}

	@Override
	public Publisher findOne(Publisher publisher) {
		return publisherRepository.findOne(publisher.getId());
	}

	@Override
	public List<Publisher> findAll() {
		return publisherRepository.findAll();
	}

	@Override
	public Publisher findById(long id) {
		return publisherRepository.findById(id);
	}

}
