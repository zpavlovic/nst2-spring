package com.nst.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;

import com.nst.model.Category;

public interface CategoryService {
	
	Category save(Category category);

	void delete(Category category);

	Category findOne(Category category);

	List<Category> findAll();

	Category findById(long id);

	List<Category> find5Categories();

}
