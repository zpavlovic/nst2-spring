package com.nst.repository;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nst.model.Author;
import com.nst.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	
	@Query("SELECT c FROM Category c WHERE c.id = ?1")
	Category findById(long id);
	
	@Query("SELECT c FROM Category c")
	List<Category> find5Categories(Pageable pageable);

}
