package com.nst.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nst.model.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {
	
	@Query("SELECT p FROM Publisher p WHERE p.id = ?1")
	Publisher findById(long id);

}
