package com.nst.repository;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nst.model.Author;
import com.nst.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	
	List<Book> findByNameContaining(String value);
	
	@Query("SELECT b FROM Book b WHERE b.id = ?1")
	Book findById(long id);
	
	@Query("SELECT b FROM Book b ORDER BY b.views DESC")
	List<Book> getPopularBooks(Pageable pagable);
	
	
	@Query("SELECT b FROM Book b")
	List<Book> reccommendBooks(Pageable pagable);
	
	@Query("SELECT b FROM Book b ORDER BY b.id DESC")
	List<Book> findAll();
	
}
