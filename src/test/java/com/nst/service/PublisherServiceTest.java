package com.nst.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.nst.Nst2ApplicationTests;
import com.nst.model.Publisher;
import com.nst.model.User;

public class PublisherServiceTest extends Nst2ApplicationTests {
	
	@Autowired
	PublisherService publisherService;
	
	@Autowired
	UserService userService;

	@Test
	public void testSave() {
		//Save user
		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		
		//Save publisher with user
		Publisher publisher = new Publisher(1, "Test publisher", "test.com", savedUser);
		Publisher savedPublisher = publisherService.save(publisher);
		
		//Check if it is not null
		assertNotNull(savedPublisher);
	}
	
	@Test
	public void testUpdatePublisher(){
		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		
		//Save publisher with user
		Publisher publisher = new Publisher(1, "Test publisher", "test.com", savedUser);
		Publisher savedPublisher = publisherService.save(publisher);
		
		savedPublisher.setName("Updated");
		Publisher updatedPublisher = publisherService.save(savedPublisher);
		
		assertEquals(updatedPublisher.getId(), savedPublisher.getId());
	}
	
	@Test
	public void testDeletePublisher(){
		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		
		//Save publisher with user
		Publisher publisher = new Publisher(2, "Test publisher", "test.com", savedUser);
		Publisher savedPublisher = publisherService.save(publisher);
		
		publisherService.delete(savedPublisher);
		
		Publisher deletedPublisher = publisherService.findOne(savedPublisher);
		
		assertNull(deletedPublisher);
	}
	
	@Test
	public void testFindAllPublishers(){
		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		
		//Save publisher with user
		Publisher publisher = new Publisher(2, "Test publisher", "test.com", savedUser);
		publisherService.save(publisher);
		
		List<Publisher> publishers = publisherService.findAll();
		
		assertThat(publishers.size(), is(2));
	}

}
