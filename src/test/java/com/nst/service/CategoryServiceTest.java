package com.nst.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import com.nst.Nst2ApplicationTests;
import com.nst.model.Category;
import com.nst.model.User;

public class CategoryServiceTest extends Nst2ApplicationTests {
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private UserService userService;

	@Test
	public void testSaveCategory() {		
		Category savedCategory = saveCategory();
		assertNotNull(savedCategory);		
	}
	
	@Test
	public void testDeleteCategory() {		
		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		Category savedCategory = categoryService.save(new Category(5, "Test category", "Test", savedUser));
		assertNotNull(savedCategory);
		
		categoryService.delete(savedCategory);
		Category deletedCategory = categoryService.findOne(savedCategory);
		assertNull(deletedCategory);		
	}
	
	@Test
	public void testUpdatedCategory() {		
		Category savedCategory = saveCategory();
		assertNotNull(savedCategory);
		
		Category updatedCategory = categoryService.save(savedCategory);
		assertNotNull(updatedCategory);
		assertEquals(savedCategory, updatedCategory);		
	}
	
	@Test
	public void testFindAllCategories(){
		Category savedCategory = saveCategory();
		assertNotNull(savedCategory);
		
		List<Category> categories = categoryService.findAll();
		assertThat(categories.isEmpty(), is(false));
	}
	
	public Category saveCategory(){
		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		Category savedCategory = categoryService.save(new Category(1, "Test category", "Test", savedUser));
		return savedCategory;
	}
	
	@Test
	public void find5Categories(){
		
		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		Category savedCategory = categoryService.save(new Category(1, "Test category", "Test", savedUser));
		
		Category savedCategory1 = categoryService.save(new Category(2, "Test category", "Test", savedUser));
		
		List<Category> categories = categoryService.find5Categories();
		
		assertThat(categories.size(), is(2));
		
	}

}
