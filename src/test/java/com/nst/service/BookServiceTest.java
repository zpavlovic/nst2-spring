package com.nst.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import com.nst.Nst2ApplicationTests;
import com.nst.model.Author;
import com.nst.model.Book;
import com.nst.model.Category;
import com.nst.model.Publisher;
import com.nst.model.User;

import javassist.NotFoundException;


public class BookServiceTest extends Nst2ApplicationTests {

	@Autowired
	private BookService bookService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private PublisherService publisherService;

	@Rule
	public final ExpectedException exception = ExpectedException.none();


	@Test
	public void testSaveBook() {

		User currentUser = new User(1, "admin", "admin", "admin", "admin");

		List<Category> categories = setUpCategory(currentUser);

		List<Author> authors = setUpAuthor(currentUser);

		java.sql.Date date = setUpDates();	

		Book savedBook = saveBook(currentUser, date, date, authors, categories, 1);

		assertNotNull(savedBook);
	}

	@Test
	public void testDeleteBook(){

		User currentUser = new User(1, "admin", "admin", "admin", "admin");

		List<Category> categories = setUpCategory(currentUser);

		List<Author> authors = setUpAuthor(currentUser);

		java.sql.Date date = setUpDates();	

		Book savedBook = saveBook(currentUser, date, date, authors, categories, 1);

		assertNotNull(savedBook);

		bookService.delete(savedBook);

		Book deletedBook = bookService.findOne(savedBook);

		assertNull(deletedBook);
	}

	@Test
	public void testUpdateBook(){

		User currentUser = new User(1, "admin", "admin", "admin", "admin");

		List<Category> categories = setUpCategory(currentUser);

		List<Author> authors = setUpAuthor(currentUser);

		java.sql.Date date = setUpDates();	

		Book savedBook = saveBook(currentUser, date, date, authors, categories, 1);

		assertNotNull(savedBook);	

		Book updateBook = bookService.findOne(savedBook);
		updateBook.setName("New name for update");

		assertNotNull(updateBook);
		assertEquals(savedBook, updateBook);
	}

	@Test
	public void findAllBooks(){
		User currentUser = new User(1, "admin", "admin", "admin", "admin");

		List<Category> categories = setUpCategory(currentUser);

		List<Author> authors = setUpAuthor(currentUser);

		java.sql.Date date = setUpDates();	

		Book savedBook = saveBook(currentUser, date, date, authors, categories, 1);

		assertNotNull(savedBook);	

		List<Book> books = bookService.findAll();
		System.out.println("BOOKS SIZE: "+String.valueOf(books.size()));
		assertThat(books.isEmpty(), is(false));
	}

	@Test
	public void testBookSearch(){

		//Add book to database
		User currentUser = new User(1, "admin", "admin", "admin", "admin");		
		List<Category> categories = setUpCategory(currentUser);		
		List<Author> authors = setUpAuthor(currentUser);		
		java.sql.Date date = setUpDates();			
		Book savedBook = saveBook(currentUser, date, date, authors, categories, 1);	
		assertNotNull(savedBook);	

		try {
			List<Book> books = bookService.search("name");
			assertNotNull(books);
			assertThat(books.isEmpty(), is(false));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Book search fail");
		}
	}

	@Test
	public void testBookSearchEmpty() throws Exception{
		exception.expect(Exception.class);
		bookService.search("Book name111");
	}

	@Test
	public void testIncreaseBookViews(){
		//Add book to database
		User currentUser = new User(1, "admin", "admin", "admin", "admin");		
		List<Category> categories = setUpCategory(currentUser);		
		List<Author> authors = setUpAuthor(currentUser);		
		java.sql.Date date = setUpDates();			
		Book savedBook = saveBook(currentUser, date, date, authors, categories, 1);	
		assertNotNull(savedBook);	
				
		//Increase views
		long views = savedBook.getViews();
		views++;
		savedBook.setViews(views);
				
		//Get updated book and test it
		Book updatedBook = bookService.saveBook(savedBook);
		assertNotNull(updatedBook);
		assertEquals(updatedBook, savedBook);
	}
	
	@Test
	public void testFindOneById(){
		//Add book to database
		User currentUser = new User(1, "admin", "admin", "admin", "admin");		
		List<Category> categories = setUpCategory(currentUser);		
		List<Author> authors = setUpAuthor(currentUser);		
		java.sql.Date date = setUpDates();			
		Book savedBook = saveBook(currentUser, date, date, authors, categories, 1);	
		assertNotNull(savedBook);	
		
		Book findBook = bookService.findById(savedBook.getId());
		assertNotNull(findBook);
		assertEquals(findBook, savedBook);
	}
	
	@Test
	public void testGetTop5Books(){
		User currentUser = new User(1, "admin", "admin", "admin", "admin");		
		List<Category> categories = setUpCategory(currentUser);		
		List<Author> authors = setUpAuthor(currentUser);		
		java.sql.Date date = setUpDates();
		
		
		Book firstBook = saveBook(currentUser, date, date, authors, categories, 30);	
		assertNotNull(firstBook);	
		
		Book secondBook = saveBook(currentUser, date, date, authors, categories, 333);	
		assertNotNull(secondBook);	
		

		Book thirdBook = saveBook(currentUser, date, date, authors, categories, 20);	
		assertNotNull(thirdBook);	
		
		Book fourthBook = saveBook(currentUser, date, date, authors, categories, 100);	
		assertNotNull(fourthBook);	
		

		Book fifthBook = saveBook(currentUser, date, date, authors, categories, 1);	
		assertNotNull(fifthBook);	
		
		Book sixthBook = saveBook(currentUser, date, date, authors, categories, 100);	
		assertNotNull(sixthBook);	

		Book seventhBook = saveBook(currentUser, date, date, authors, categories, 2);	
		assertNotNull(seventhBook);	
		
		Book eightBook = saveBook(currentUser, date, date, authors, categories, 200);	
		assertNotNull(eightBook);	
		
		List<Book> popularBooks = bookService.getPopularBooks();
		System.out.println("SIZE: "+popularBooks.size());
		assertThat(popularBooks.isEmpty(), is(false));
	}
	
	@Test
	public void testFindAllBooksByAuthor(){
		//Add author to database
		User currentUser = new User(1, "admin", "admin", "admin", "admin");		
		List<Category> categories = setUpCategory(currentUser);		
		List<Author> authors = setUpAuthor(currentUser);		
		java.sql.Date date = setUpDates();
		
		
		Book firstBook = saveBook(currentUser, date, date, authors, categories, 30);	
		assertNotNull(firstBook);	
		
		Book secondBook = saveBook(currentUser, date, date, authors, categories, 333);	
		assertNotNull(secondBook);	
		
		List<Book> booksByAuthor = authors.get(0).getBookList();
		
		assertNotNull(booksByAuthor);
		
		
		
	}
	


	private Book saveBook(User currentUser, Date publishedDate, Date addedDate, List<Author> authors, List<Category> categories, long views){
		Publisher publisher = new Publisher(1, "test publisher", "test.com", currentUser);
		Publisher savedPublisher = publisherService.save(publisher);
		Book book = 
				new Book(1, "name", "opis", null, publishedDate, addedDate, 111, views, "short desc", 111, 222, "www.test.com", currentUser, authors, categories, savedPublisher);
		Book savedBook = bookService.saveBook(book);
		return savedBook;
	}

	private Date setUpDates() {
		Calendar calendar = Calendar.getInstance();
		java.sql.Date date = new java.sql.Date(calendar.getTime().getTime());
		return date;
	}

	private List<Author> setUpAuthor(User currentUser) {
		Author savedAuthor = authorService.save(new Author(1, "Ime autora", "Test", "Opis", currentUser));
		assertNotNull(savedAuthor);
		List<Author> authorsList = new ArrayList<>();
		authorsList.add(savedAuthor);
		return authorsList;
	}

	public List<Category> setUpCategory(User currentUser){
		User savedUser = userService.save(currentUser);
		Category savedCategory = categoryService.save(new Category(1, "Test category", "Test", savedUser));
		assertNotNull(savedCategory);	
		List<Category> categoryList = new ArrayList<>();
		categoryList.add(savedCategory);
		return categoryList;
	}

}
