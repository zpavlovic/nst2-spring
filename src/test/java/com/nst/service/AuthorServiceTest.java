package com.nst.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.nst.Nst2ApplicationTests;
import com.nst.model.Author;
import com.nst.model.Category;
import com.nst.model.User;

public class AuthorServiceTest extends Nst2ApplicationTests{

	@Autowired
	AuthorService authorService;

	@Autowired
	private UserService userService;


	@Test
	public void testSaveAuthor() {

		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		Author savedAuthor = authorService.save(new Author(1, "Ime autora", "Test", "Opis", savedUser));
		assertNotNull(savedAuthor);

	}

	@Test
	public void testDeleteAuthor(){

		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		Author savedAuthor = authorService.save(new Author(5, "Ime autora", "Test", "Opis", savedUser));
		assertNotNull(savedAuthor);

		authorService.delete(savedAuthor);		
		Author deletedAuthor = authorService.findOne(savedAuthor);		
		assertNull(deletedAuthor);

	}

	@Test
	public void testUpdateAuthor(){

		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		Author savedAuthor = authorService.save(new Author(1, "Ime autora", "Test", "Opis", savedUser));
		assertNotNull(savedAuthor);

		Author updateAuthor = authorService.save(savedAuthor);
		updateAuthor.setFirstName("New name");

		assertNotNull(updateAuthor);
		assertEquals(savedAuthor, updateAuthor);		

	}

	@Test
	public void testFindAllAuthors(){

		//Add author to database
		User currentUser = new User(1, "admin", "admin", "admin", "admin");
		User savedUser = userService.save(currentUser);
		Author savedAuthor = authorService.save(new Author(1, "Ime autora", "Test", "Opis", savedUser));
		assertNotNull(savedAuthor);

		//Find all
		List<Author> authors = authorService.findAll();
		System.out.println("AUTHORS SIZE: "+String.valueOf(authors.size()));
		assertNotNull(authors);

		//Check if authors list is not empty. We should have 1 element
		assertThat(authors.isEmpty(), is(false));

	}



}
