/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.7.11-log : Database - nst2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nst2` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `nst2`;

/*Table structure for table `author` */

CREATE TABLE `author` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `description` text,
  `image` text,
  `website` text,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_author_user_id` (`user_id`),
  CONSTRAINT `fk_author_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

/*Data for the table `author` */

insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (25,'Robert C.','Martin','Uncle Bob',NULL,'cleancoder.com',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (27,'Marko','Markovic','opis',NULL,'opis.com',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (28,'Jason','Hickey','Jason Hickey is a Software Engineer at Google, where he is responsible for Computing Infrastructure. His research interests include programming languages, formal methods, and compilers for fault-tolerant distributed systems and high-conﬁdence control. He was an assistant professor at California Institute of Technology (Caltech) until 2008, where he developed new courses in modern operating systems, compilers, and programming language semantics.','jasonhickey.jpg','https://www.linkedin.com/in/jason-hickey-5141148',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (29,'Fintan ','Culwin','',NULL,'http://myweb.lsbu.ac.uk/~fintan/',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (31,'eeee','eee','eee','1469771915-0193e02.jpg','eee',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (32,'Dd','d','',NULL,'',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (33,'Novi test author','Test','test',NULL,'test',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (34,'test','test','',NULL,'',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (35,'Cody ','Lindley','Cody Lindley is a front-end/JavaScript developer and recovering Flash developer. He has an extensive background working professionally (18+ years) with HTML, CSS, JavaScript, and client-side performance techniques as it pertains to web development. Currently he is working for Telerik (focused on Kendo UI) as a Developer Advocate.','cody.jpg','http://codylindley.com/',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (36,'Abraham','Kuri','Abraham Kuri is a Rails developer with 5 years experience. His experience includes working as a freelancer building software products and more recently to collaborate into the open source community. He developed Furatto a front end framework built with Sass, Sabisu the next generation api explorer for your Rails app and have collaborated on other projects. He is a Computer Science graduate of ITESM and founded two companies in Mexico (Icalia Labs and Codeando Mexico).','abraham.jpeg','https://twitter.com/kurenn',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (37,'Neil','Gray','A lecturer at the Department of Computer Science, University of Wollongong, Australia.','1458131486-nabg0_cr.jpg','http://www.uow.edu.au/~nabg/n.html',1);
insert  into `author`(`id`,`first_name`,`last_name`,`description`,`image`,`website`,`user_id`) values (38,'James','W. Cooper','James W. Cooper was a chemistry professor at Tufts University, and spent 25 years in IBM\'s Research Division, and 3 years as a Vice President of Bruker Instruments. He holds a Ph.D. in organic chemistry from Ohio State University and an A.B. from Oberlin College.   Back to all authors','jamescooper.jpg','',1);

/*Table structure for table `book` */

CREATE TABLE `book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `image` text,
  `published_date` date DEFAULT NULL,
  `added_date` date NOT NULL,
  `pages_number` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `views` bigint(20) DEFAULT '0',
  `short_description` text,
  `isbn_10` int(10) DEFAULT NULL,
  `isbn_13` int(13) DEFAULT NULL,
  `link` text NOT NULL,
  `publisher_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_book_user_id` (`user_id`),
  KEY `fk_book_publisher_id` (`publisher_id`),
  CONSTRAINT `fk_book_publisher_id` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`id`),
  CONSTRAINT `fk_book_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

/*Data for the table `book` */

insert  into `book`(`id`,`name`,`description`,`image`,`published_date`,`added_date`,`pages_number`,`user_id`,`views`,`short_description`,`isbn_10`,`isbn_13`,`link`,`publisher_id`) values (81,'Introduction to Objective Caml','Excerpts from the Preface:  Objective Caml (OCaml) is a popular, expressive, high-performance dialect of ML developed by a research team at INRIA in France. This book presents a practical introduction and guide to the language, with topics ranging from how to write a program to the concepts and conventions that affect how affect how programs are developed in OCaml. The text can be divided into three main parts.  - The core language (Chapters 2–10). - The module system (Chapters 11–13). - Objects and class (Chapters 14–17).  This sequence is intended to follow the ordering of concepts needed as programs grow in size (though objects and classes can be introduced at any point in the development). It also happens to follow the history of Caml: many of the core concepts were present in Caml and Caml Light in the mid-1980s and early 1990s; Caml Special Light introduced modules in 1995; and Objective Caml added objects and classes in 1996.  Intended Audience:  This book is intended for programmers, undergraduate and beginning graduate students with some experience programming in a procedural programming language like C or Java, or in some other functional programming language. Some knowledge of basic data structures like lists, stacks, and trees is assumed as well.  The exercises vary in difficulty. They are intended to provide practice, as well as to investigate language concepts in greater detail, and occasionally to introduce special topics not present elsewhere in the text.',NULL,'2008-01-01','2016-08-26',0,1,52,'This book presents a practical introduction and guide to Objective Caml, with topics ranging from how to write a program to the concepts and conventions that affect how programs are developed in Objective Caml.',0,0,'http://courses.cms.caltech.edu/cs134/cs134b/book.pdf',NULL);
insert  into `book`(`id`,`name`,`description`,`image`,`published_date`,`added_date`,`pages_number`,`user_id`,`views`,`short_description`,`isbn_10`,`isbn_13`,`link`,`publisher_id`) values (82,'Java: An Object First Approach','This is a collection of lectures in the on-line course Introduction to Object-Oriented Programming Using C++. In this course, object-orientation is introduced as a new programming concept which should help you in developing high quality software. Object-orientation is also introduced as a concept which makes developing of projects easier.   However, this is not a course for learning the C++ programming language. If you are interested in learning the language itself, you might want to go through other tutorials, such as C++: Annotations by Frank Brokken and Karel Kubat. In this tutorial only those language concepts that are needed to present coding examples are introduced.','1460207902-716GWERFBRL._SX368_BO1,204,203,200_.jpg','1997-10-29','2016-08-26',0,1,16,'A thorough introduction to the production of software artefacts, a process known as software development, using Java programming language.',0,0,'http://burks.bton.ac.uk/burks/language/java/jof/jflconte.htm',NULL);
insert  into `book`(`id`,`name`,`description`,`image`,`published_date`,`added_date`,`pages_number`,`user_id`,`views`,`short_description`,`isbn_10`,`isbn_13`,`link`,`publisher_id`) values (92,'JavaScript Enlightenment','This book is not about JavaScript design patterns or implementing an object-oriented paradigm with JavaScript code. It was not written to distinguish the good features of the JavaScript language from the bad. It is not meant to be a complete reference guide. It is not targeted at people new to programming or those completely new to JavaScript. Nor is this a cookbook of JavaScript recipes. Those books have been written.  It was my intention to write a book to give the reader an accurate JavaScript worldview through an examination of native JavaScript objects and supporting nuances: complex values, primitive values, scope, inheritance, the head object, etc. I intend this book to be a short and digestible summary of the ECMA-262, Edition 3 specification, focused on the nature of objects in JavaScript.  If you are a designer or developer who has only used JavaScript under the mantle of libraries (such as jQuery, Prototype, etc), it is my hope that the material in this book will transform you from a JavaScript library user into a JavaScript developer.','1469772346-41pZo24FNvL._SX377_BO1,204,203,200_.jpg','2013-01-10','2016-08-26',141,1,3,'This book gives readers an accurate JavaScript worldview under the hood and intends to transform them from JavaScript library users into JavaScript developers.',1449342884,0,'http://javascriptenlightenment.com/JavaScript_Enlightenment.pdf',NULL);
insert  into `book`(`id`,`name`,`description`,`image`,`published_date`,`added_date`,`pages_number`,`user_id`,`views`,`short_description`,`isbn_10`,`isbn_13`,`link`,`publisher_id`) values (93,'APIs on Rails: Building REST APIs with Rails','Welcome to APIs on Rails a tutorial on steroids on how to buid your next API with Rails. The goal of this book is to provide an answer on how to develop a RESTful API following the best practices out there, along with my own experience. By the time you are done with API’s on Rails you should be able to build your own API and integrate it with any clients such as a web browser or your next mobile app. The code generated is built on top of Rails 4 which is the current version, for more information about this check out http://rubyonrails.org/. The most up-to-date version of the API\'s on Rails can be found on http://apionrails.icalialabs.com; don’t forget to update your offline version if that is the case.  The intention with this book it’s not teach just how to build an API with Rails rather to teach you how to build scalable and maintanable API with Rails, which means taking your current Rails knowledge to the next level when on this approach. In this journey we are going to take, you will learn to:  Build JSON responses Use Git for version controlling Testing your endpoints Optimize and cache the API','apionrails.jpg','2014-01-01','2016-08-26',0,1,13,'The best way to build scalable and maintanable API with Rails.',0,0,'http://apionrails.icalialabs.com/book',NULL);
insert  into `book`(`id`,`name`,`description`,`image`,`published_date`,`added_date`,`pages_number`,`user_id`,`views`,`short_description`,`isbn_10`,`isbn_13`,`link`,`publisher_id`) values (94,'A Beginners C++','A Beginners C++ is primarily intended for introductory Computer Science courses that use C++ as an implementation language. However, the book should be equally suited to an individual who wants to learn how to program their own personal computer.  This book assumes that you are a computer literate. You are required to have experience with word processing packages, and possibly other packages like spreadsheets and data bases. Although most schools do provide a limited introduction to programming (usually in Pascal or possibly a dialect of Basic), this book does not rely on such past experience.  It is expected that you will use one of the modern Integrated Development Environments (IDE) on a personal computer. Examples of such environments include the Borland environment for Intel PCs and the Symantec environment for Macintosh/PowerPC machines.','beginnercplusplus.jpg','2002-12-31','2016-08-26',0,1,3,'A reference for introductory Computer Science courses that use C++ as an implementation language.',0,0,'http://www.uow.edu.au/~nabg/ABC/ABC.html',NULL);
insert  into `book`(`id`,`name`,`description`,`image`,`published_date`,`added_date`,`pages_number`,`user_id`,`views`,`short_description`,`isbn_10`,`isbn_13`,`link`,`publisher_id`) values (95,'The Design Patterns: Java Companion','It could be somewhat off-putting, but design patterns are just convenient ways of reusing object-oriented code between projects and between programmers. The idea behind design patterns is simple -- write down and catalog common interactions between objects that programmers have frequently found useful.  Design Patterns is a catalog of 23 generally useful patterns for writing object-oriented software. It is written as a catalog with short examples and substantial discussions of how the patterns can be constructed and applied. Each of the 23 patterns is presented with at least one complete, visual Java program.  Nearly all of the example programs in this book use the JFC to produce the interfaces the example code. Since not everyone may be familiar with these classes, and since the the intend is to build some basic classes from the JFC to use throughout the examples, there will be a short break after introducing the creational patterns and the readers will spend a chapter introducing the JFC. While the chapter is not a complete tutorial in every aspect of the JFC, it does introduce the most useful interface controls and shows how to use them.','aaa.jpg','2000-02-13','2016-08-27',351,1,18,'A catalog of 23 generally useful patterns for writing object-oriented software, each of them is presented with at least one complete, visual Java program.',201485397,0,'http://www.unc.edu/~stotts/comp723/JavaDPbook.pdf',NULL);

/*Table structure for table `book_author` */

CREATE TABLE `book_author` (
  `book_id` bigint(20) DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  KEY `fk_book_author_book_id` (`book_id`),
  KEY `fk_book_author_author_id` (`author_id`),
  CONSTRAINT `fk_book_author_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `fk_book_author_user_id` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `book_author` */

insert  into `book_author`(`book_id`,`author_id`) values (92,35);
insert  into `book_author`(`book_id`,`author_id`) values (94,37);
insert  into `book_author`(`book_id`,`author_id`) values (93,36);
insert  into `book_author`(`book_id`,`author_id`) values (81,28);
insert  into `book_author`(`book_id`,`author_id`) values (82,29);
insert  into `book_author`(`book_id`,`author_id`) values (95,28);

/*Table structure for table `book_category` */

CREATE TABLE `book_category` (
  `book_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  KEY `fk_book_category_book_id` (`book_id`),
  KEY `fk_book_category_category_id` (`category_id`),
  CONSTRAINT `fk_book_category_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_book_category_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `book_category` */

insert  into `book_category`(`book_id`,`category_id`) values (92,35);
insert  into `book_category`(`book_id`,`category_id`) values (94,38);
insert  into `book_category`(`book_id`,`category_id`) values (93,36);
insert  into `book_category`(`book_id`,`category_id`) values (93,37);
insert  into `book_category`(`book_id`,`category_id`) values (81,29);
insert  into `book_category`(`book_id`,`category_id`) values (82,30);
insert  into `book_category`(`book_id`,`category_id`) values (95,28);

/*Table structure for table `category` */

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category_user_id` (`user_id`),
  CONSTRAINT `fk_category_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

/*Data for the table `category` */

insert  into `category`(`id`,`name`,`description`,`user_id`) values (28,'Introduction to Computer Science','An introduction to the study of the theoretical foundations of information and computation and their implementation and application in computer systems.   All categories',1);
insert  into `category`(`id`,`name`,`description`,`user_id`) values (29,'Functional Programming','A style of programming that emphasizes the evaluation of expressions, rather than execution of commands. The expressions in these language are formed by using functions to combine basic values.   All categories',1);
insert  into `category`(`id`,`name`,`description`,`user_id`) values (30,'Object Oriented Programming','A programming paradigm that uses objects to design applications and computer programs. It utilizes several techniques from previously established paradigms, including inheritance, modularity, polymorphism, and encapsulation. ',1);
insert  into `category`(`id`,`name`,`description`,`user_id`) values (35,'JavaScript','JavaScript is a scripting language most often used for client-side web development. JavaScript is a dynamic, weakly typed, prototype-based language with first-class functions. JavaScript was influenced by many languages and was designed to have a similar look to Java, but be easier for non-programmers to work with.',1);
insert  into `category`(`id`,`name`,`description`,`user_id`) values (36,'Ruby','A reflective, dynamic, single-pass interpreted, object-oriented programming language that combines syntax inspired by Perl with Smalltalk-like object-oriented features, and also shares some features with Python, Lisp, Dylan and CLU.',1);
insert  into `category`(`id`,`name`,`description`,`user_id`) values (37,'Web Design and Development','Learn the methods and tools for the design, development and maintenance of website. ',1);
insert  into `category`(`id`,`name`,`description`,`user_id`) values (38,'C++','A general-purpose, high-level programming language with low-level facilities. It is a statically typed free-form multi-paradigm language supporting procedural programming, data abstraction, object-oriented programming, generic programming and RTTI.',1);
insert  into `category`(`id`,`name`,`description`,`user_id`) values (39,'Java','An object-oriented applications programming language developed by Sun Microsystems in the early 1990s. Its applications are typically compiled to bytecode, although compilation to native machine code is also possible.   All categories',1);
insert  into `category`(`id`,`name`,`description`,`user_id`) values (40,'Python','A high-level programming language designed around a philosophy which emphasises the importance of programmer effort over computer effort, and it rejects more arcane language features, prioritising readability over speed or expressiveness.   All categories',1);
insert  into `category`(`id`,`name`,`description`,`user_id`) values (41,'PHP','A reflective programming language originally designed for producing dynamic web pages. It is used mainly in server-side scripting, but can be used from a command line interface or in standalone graphical applications.   All categories',1);

/*Table structure for table `publisher` */

CREATE TABLE `publisher` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `website` text,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_publisher_user_id` (`user_id`),
  CONSTRAINT `fk_publisher_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `publisher` */

insert  into `publisher`(`id`,`name`,`website`,`user_id`) values (2,'Test publisher','test.com',1);
insert  into `publisher`(`id`,`name`,`website`,`user_id`) values (4,'Testiranje','http://balkanandroid.com/',1);
insert  into `publisher`(`id`,`name`,`website`,`user_id`) values (5,'O’Reilly Media, Inc.','http://www.oreilly.com/',1);
insert  into `publisher`(`id`,`name`,`website`,`user_id`) values (6,'John Wiley & Sons','http://eu.wiley.com/WileyCDA/Section/index.html',1);
insert  into `publisher`(`id`,`name`,`website`,`user_id`) values (7,'Addison-Wesley Professional','http://www.awprofessional.com/',1);

/*Table structure for table `user` */

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`first_name`,`last_name`) values (1,'admin','1','admin','admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
